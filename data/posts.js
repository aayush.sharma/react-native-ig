import { USERS } from "./users";

export const POSTS = [
    {
        "imageUrl" : "https://pbs.twimg.com/media/FVJJKFAWQAAyDB2.jpg",
        "user":USERS[2].user,
        "likes":"10280",
        "caption":"Thor : love and thunder, in cinemas from 7th july 2022",
        "profile_picture":USERS[2].image,
        "comments":[
            {
                "user":"Aayush Sharma",
                "comment":"Very excited to see this movie, little disapointed to see Gorr's CGI"
            },
            {
                "user":"ComicVerse",
                "comment":"Story seems to be comic book accurate but let see what happens in the movie"
            }
        ]
    }
]