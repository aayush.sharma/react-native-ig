import { View, Text, ScrollView, StyleSheet, Image } from 'react-native'
import React from 'react';
import { USERS } from '../../data/users';

const Stories = () => {
    return (
        <View style={{ marginBottom: 13 }}>
            <ScrollView
                horizontal
                showsHorizontalScrollIndicator={false}
            >
                {USERS.map((story, index) => (
                    <View style={styles.storyView} key={index}>
                        <Image
                            style={styles.story}
                            source={{ uri: story.image }}
                        />
                        <Text style={{ color: "white", flex: 1 }} numberOfLines={1} ellipsizeMode="tail">{story.user}</Text>
                    </View>
                ))}
            </ScrollView>
            {/* <Text style={{ color: "white" }}>Stories</Text> */}
        </View>
    )
}

const styles = StyleSheet.create({
    storyView: {
        width: 90,
        height: 90,
        alignItems: "center",
        justifyContent: "center"
    },
    story: {
        width: 70,
        height: 70,
        borderRadius: 50,
        borderWidth: 3,
        // marginLeft: 6,
        borderColor: "#ff8501"
    }
})

export default Stories