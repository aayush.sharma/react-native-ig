import { View, Text, Image, StyleSheet } from 'react-native'
import React from 'react'
import { Divider } from 'react-native-elements'

const Posts = ({ post }) => {
    return (
        <View style={{ marginBottom: 30 }}>
            <Divider width={1} color={'white'} orientation={"horizontal"} />
            <PostHeader post={post} />
            <PostImage post={post}/>
        </View>
    )
}

const PostHeader = ({ post }) => {
    return (
        <View style={{ margin: 6, flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
            <View style={{ flexDirection: "row", alignItems: "center" }}>
                <Image
                    style={styles.story}
                    source={{ uri: post.profile_picture }}
                />
                <Text style={styles.text}>{post.user}</Text>
            </View>
            <View>
                <Text style={{color:'white',fontSize:17,fontWeight:900}}>...</Text>
            </View>
        </View>
    )
}

const PostImage = ({post}) => (
    <View style={{width:"100%",height:450}}>
        <Image source={{uri:post.imageUrl}} style={{height:"100%",resizeMode:"cover"}}/>
    </View>
)

const styles = StyleSheet.create({
    story: {
        width: 45,
        height: 45,
        borderRadius: 50,
        borderWidth: 1.5,
        marginLeft: 6,
        borderColor: "#ff8501"
    },
    text: {
        color: "white",
        fontSize: 14,
        fontWeight: 400,
        marginLeft: 8
    }
})

export default Posts